<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon-gendarmerie.png">
    <title>Brigade Numérique | Gendarmerie</title>
    <!-- This page CSS -->
    <link href="../../assets/libs/jquery-steps/jquery.steps.css" rel="stylesheet">
    <link href="../../assets/libs/jquery-steps/steps.css" rel="stylesheet">
    <!-- FAQ CSS -->
    <link rel="stylesheet" href="faq/style.css">
	<!-- Contact CSS -->
	<link rel="stylesheet" href="contact/main.css">
    <!-- Custom CSS -->
    <link href="../../dist/css/style.min.css" rel="stylesheet">
    <!-- Font -->
    <style>
        @import url('https://fonts.googleapis.com/css2?family=IBM+Plex+Sans:wght@500&display=swap');
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header">
                    <!-- This is for the sidebar toggle which is visible on mobile only -->
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)">
                        <i class="ti-menu ti-close"></i>
                    </a>
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <div class="navbar-brand">
                        <a href="index.html" class="logo">
                            <!-- Logo icon -->
                            <b class="logo-icon">
                                <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                                <!-- Logo icon -->
                                <img src="../../assets/images/logo-crogend.jfif" alt="homepage" width="30%"/>
                            </b>
                            <!--End Logo icon -->
                        </a>
                    </div>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->    
                </div>
                <div class="navbar-collapse collapse" id="navbarSupportedContent">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-right m-auto">
                        <li class="nav-item d-none d-md-block">
                            <a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar">
                                <i class="mdi mdi-menu font-24"></i>
                            </a>
                        </li> 
                        <li class="align-self-center">
                                <h1 class="card-title font-weight-bold" style="font-family:'IBM Plex Sans', sans-serif;"> Brigade Numérique</h1>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Barre de recherche -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-left">
                        <li class="nav-item search-box">
                            <a class="nav-link waves-effect waves-dark" href="javascript:void(0)" >
                                <div class="d-flex align-items-center">
                                    <i class="mdi mdi-magnify font-20 mr-1"></i>
                                    <div class="ml-1 d-none d-sm-block">
                                        <span>Rechercher</span>
                                    </div>
                                </div>
                            </a>
                            <form class="app-search position-absolute">
                                <input type="text" class="form-control" placeholder="Rechercher ...">
                                <a class="srh-btn">
                                    <i class="ti-close"></i>
                                </a>
                            </form>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Right side Logo Gendarmerie -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-right">
                        <!-- ============================================================== -->
                        <!-- LOGO Gendarmerie -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a href="javascript:void(0)" class="logo">
                                <!-- Logo icon -->
                                <b class="logo-icon">
                                    <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                                    <!-- Logo icon -->
                                    <img class="float-right" src="../../assets/images/favicon-gendarmerie.png" alt="homepage" width="30%"/>
                                </b>
                                <!--End Logo icon -->
                            </a>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End LOGO CROGEND -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->

        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="index.html" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Accueil </span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link two-column waves-effect waves-dark" href="pre-plainte-form.html" aria-expanded="false"><i class="mdi mdi-fingerprint"></i><span class="hide-menu">Pré-Plainte </span></a></li>
                        <li class="sidebar-item mega-dropdown"> <a class="sidebar-link waves-effect waves-dark" href="signalement-form.html" aria-expanded="false"><i class="mdi mdi-alert-outline"></i><span class="hide-menu">Signaler un cas </span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="faq.html" aria-expanded="false"><i class="mdi mdi-comment-question-outline"></i><span class="hide-menu">FAQ</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="page-contact.html" aria-expanded="false"><i class="mdi mdi-email-outline"></i><span class="hide-menu">Contact</span></a></li>
                
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
         <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Pré-Plainte</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="index.html">Accueil</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Pré-Plainte</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Section -->
                <!-- ============================================================== -->
                <div class="row actu">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body wizard-content">
                                <h4 class="cti-h4">Formulaire de pré-plainte <i class="ct-i mdi mdi-pencil"></i></h4>
                                <hr> 
                                <h6 class="card-subtitle">Pour que votre plainte soit enregistrée. Vous devez remplir les informations suivantes. Une fois validée, nous vous contacterons pour signer votre déclaration au niveau de la brigade la plus proche.</h6>
                                <form action="#" class="validation-wizard wizard-circle m-t-40">
                                    <!-- Etape 1 -->
                                    <h6>Etape 1</h6>
                                    <section>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wfirstName2"> Vous déposez plainte en qualité de : <span class="text-danger">*</span> </label>
                                                        <div class="row"><input type="radio" class="col-1" id="wlastName2" name="qualitePlainte">Victime</div>
                                                        <div class="row">
                                                            <input type="radio" class="col-1" id="wlastName2" name="qualitePlainte">Représentant légal d'une personne morale <sup><i class="text-info mdi mdi-information" data-toggle="tooltip" data-placement="top" title="État, collectivités territoriales et leurs groupements, établissements publics, groupements d'intérêt public."></i></sup>
                                                        </div>
                                                        <div class="row">
                                                            <input type="radio" class="col-1" id="wlastName2" name="qualitePlainte">Représentant légal d'une personne physique <sup> <i class="text-info mdi mdi-information" data-toggle="tooltip" data-placement="top" title="Une personne physique est un être humain disposant d’une personnalité juridique, qui a donc des droits et des devoirs. Elle doit être majeure et ne pas être sous tutelle."></i></sup>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <fieldset class=" form-group border p-3"><legend class="border mt-0 p-2 text-muted h5" style="width: auto;"> <i class="mdi mdi-account"></i> Etat-civil</legend>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="wlastName2"> Sexe : <span class="text-danger">*</span> </label>
                                                        <div class="row">
                                                            <input type="radio" class="form-control required col-1" id="wlastName2" name="sexe">Homme
                                                            <input type="radio" class="form-control required col-1" id="wlastName2" name="sexe">Femme
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wfirstName2"> Prénom(s) : <span class="text-danger">*</span> </label>
                                                        <input type="text" class="form-control required" id="wfirstName2" name="firstName"> 
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wlastName2"> Nom de naissance: <span class="text-danger">*</span> </label>
                                                        <input type="text" class="form-control required" id="wlastName2" name="lastName"> 
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wlastName2"> Nom d'époux :</label>
                                                        <input type="text" class="form-control" id="wlastName2" name="EpouxName"> </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="wdate2">Date de naissance : <span class="text-danger">*</span> </label>
                                                        <input type="date" class="form-control required" id="wdate2"> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="wlocation2"> Lieu de naissance : <span class="text-danger">*</span> </label>
                                                        <input type="text" class="form-control" id="wlastName2" name="EpouxName">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wphoneNumber2">Situation Familiale : <span class="text-danger">*</span></label>
                                                        <select class="custom-select form-control required" id="wfirstName2" name="">
                                                            <option value="">Choisissez</option>
                                                            <option value="Célibataire">Célibataire</option>
                                                            <option value="Marié(e)">Marié(e)</option>
                                                            <option value="Divorcé(e)">Divorcé(e)</option>
                                                            <option value="Veuf(ve)">Veuf(ve)</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wemailAddress2"> Nationalité : <span class="text-danger">*</span> </label>
                                                        <input type="text" class="form-control required" id="wlastName2" name="nationality">  
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wemailAddress2"> Profession : <span class="text-danger">*</span> </label>
                                                        <input type="text" class="form-control required" id="wlastName2" name="profession">  
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class=" form-group border p-3"><legend class="border mt-0 p-2 text-muted h5" style="width: auto;"> <i class="mdi mdi-home"></i> Résidence</legend>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wdate2">Région : <span class="text-danger">*</span> </label>
                                                        <select class="custom-select form-control" id="wlocation2" name="location">
                                                            <option value="">Sélectionner votre région</option>
                                                            <option value="India">India</option>
                                                            <option value="USA">USA</option>
                                                            <option value="Dubai">Dubai</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wlocation2">Département :  </label>
                                                        <input type="text" class="form-control" id="wlastName2" name="commune"> 
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wlocation2">Commune :  </label>
                                                        <input type="text" class="form-control" id="wlastName2" name="commune"> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="wphoneNumber2">Adresse exacte: <span class="text-danger">*</span></label>
                                                        <input type="text" class="form-control required" id="wlastName2" name="commune"> 
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>  
                                        <small>Les champs suivis de <span class="text-danger">*</span> sont obligatoires.</small>
                                    </section> 
                                    <!-- Step 2 -->
                                    <h6>Etape 2</h6>
                                    <section>
                                        <fieldset class=" form-group border p-3"><legend class="border mt-0 p-2 text-muted h5" style="width: auto;"> <i class="mdi mdi-information-outline"></i> Les Faits et circonstances </legend>
                                            <div class="row mb-2">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="wlastName2"> Quelle est la nature de votre plainte ? : <span class="text-danger">*</span> </label>
                                                        <div class="row"><input type="radio" class="col-1" id="wlastName2" name="naturePlainte">Atteinte aux biens</div>
                                                        <div class="row"><input type="radio" class="col-1" id="wlastName2" name="naturePlainte">Atteinte aux personnes</div>
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label for="jobTitle2"> <p class="mb-0"> Motif de votre plainte :</p>
                                                            <small class="text-muted">Ex: actes de discrimination, provocation à la discrimination ou à la haine, injure ou diffamation à caractère discriminatoire...</small>
                                                        </label>
                                                        <input type="text" class="form-control required" placeholder="Indiquez le motif de votre plainte" id="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="webUrl3">Où cela s'est-il passé ? <span class="text-danger">*</span></label>
                                                        <div class="row">
                                                            <div class="col-md-7"><input type="text" class="form-control required" placeholder="Mentionnez le lieu de l'infraction"  id="" name="webUrl3"> </div>
                                                            <div class="col-md-5 mt-2"></span><input type="checkbox" class="col-1"  id="" name="webUrl3" > Je ne sais pas où cela s'est produit</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="webUrl3">Quand cela s'est-il passé ? <span class="text-danger">*</span></label>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <small class="text-muted">Veuillez indiquer la date de l'infraction</small>
                                                                <input type="date" class="form-control required" id="" name="webUrl3"> 
                                                            </div>
                                                            <div class="col-md-3">
                                                                <small class="text-muted">Veuillez indiquer l'heure approximative </small>
                                                                <input type="time" class="form-control required" id="" name="webUrl3"> 
                                                            </div>
                                                            <div class="col-md-5 mt-3"></span><input type="checkbox" class="col-1"  id="" name="webUrl3" > Je ne sais pas quand cela s'est produit</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="shortDescription3">Racontez-nous en détails les faits <span class="text-danger">*</span></label>
                                                        <textarea name="shortDescription" placeholder="Tapez votre texte ici" id="shortDescription3" rows="6" class="form-control required"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <small>Les champs suivis de <span class="text-danger">*</span> sont obligatoires.</small>
                                    </section>
                                    <!-- Step 3 -->
                                    <h6>Etape 3</h6>
                                    <section>
                                        <fieldset class=" form-group border p-3"><legend class="border mt-0 p-2 text-muted h5" style="width: auto;"> <i class="mdi mdi-account-search"></i> Preuves </legend>
                                            <div class="row mb-2">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="webUrl3"> <p class="mb-0"> Avez-vous des éléments pouvant servir à l'identification du ou des auteurs de l'infraction ? <span class="text-danger">*</span></p>
                                                            <small class="text-muted">Ex: habits, photos, vidéos, SMS, capture d'écran, email, support papier, etc...</small>
                                                         </label>
                                                        <div class="row">
                                                            <div class="col-md-7">
                                                                <input type="checkbox" class="col-1"  id="" name="webUrl3" > J'ai des éléments susceptibles d'orienter l'enquête
                                                                <textarea name="shortDescription" id="shortDescription3" placeholder="Tapez votre texte ici" rows="6" class="form-control mt-2 required"></textarea>
                                                            </div>
                                                            <div class="col-md-5 mt-2"></span><input type="checkbox" class="col-1"  id="" name="webUrl3" > Je n'ai pas d'élément susceptible d'orienter l'enquête</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class=" form-group border p-3"><legend class="border mt-0 p-2 text-muted h5" style="width: auto;"> <i class="mdi mdi-account-alert"></i> Préjudice</legend>
                                            <div class="row mb-2">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="webUrl3"> <p class="mb-0"> Avez-vous subi un préjudice moral et/ou matériel ? <span class="text-danger">*</span></p>
                                                         </label>
                                                        <div class="row">
                                                            <div class="col-md-7">
                                                                <input type="checkbox" class="col-1"  id="" name="webUrl3" > J'ai subi un préjudice moral et/ou matériel.
                                                                <p class="mb-0"><small class="text-muted">Veuillez le décrire ci-dessous</small></p>
                                                                <textarea name="shortDescription" id="shortDescription3" placeholder="Tapez votre texte ici" rows="6" class="form-control mt-2 required"></textarea>
                                                            </div>
                                                            <div class="col-md-5 mt-2"></span><input type="checkbox" class="col-1"  id="" name="webUrl3" > Je n'ai pas subi de préjudice moral et/ou matériel.</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <small>Les champs suivis de <span class="text-danger">*</span> sont obligatoires.</small> 
                                    </section> 
                                    <!-- Step 4 -->
                                    <h6>Etape 4</h6>
                                    <section>
                                        <fieldset class=" form-group border p-3"><legend class="border mt-0 p-2 text-muted h5" style="width: auto;"> <i class="mdi mdi-phone"></i> Contacts </legend>
                                            <div class="row mb-2">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="jobTitle2"> <p class="mb-0">N° Téléphone portable : <span class="text-danger">*</span> </p>
                                                            <small class="text-muted">(obligatoire) </small>
                                                        </label>
                                                        <input type="text" class="form-control required" placeholder="Tapez votre n° ici" id="">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="jobTitle2"> <p class="mb-0">N° Téléphone portable :</p>
                                                            <small class="text-muted">(facultatif) </small>
                                                        </label>
                                                        <input type="text" class="form-control required" placeholder="Tapez votre n° ici" id="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="jobTitle2">Adresse email : <span class="text-danger">*</span>
                                                        </label>
                                                        <input type="text" class="form-control required" placeholder="Tapez votre adresse email ici" id="">
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class=" form-group border p-3"><legend class="border mt-0 p-2 text-muted h5" style="width: auto;"> <i class="mdi mdi-calender"></i> Rendez-vous </legend>
                                            <div class="row mb-2">
                                                <p class="text-muted"> Vous devez signer votre déclaration à la brigade de gendarmerie de votre lieu de résidence. Pour permettre de planifier au mieux votre prise en charge en tenant compte également des impératifs de service, indiquez plusieurs dates et heures qui vous conviennent. Nous vous proposerons un rendez-vous dans les meilleurs délais. </p>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="jobTitle2">Date(s) et heure(s) de RV : <span class="text-danger">*</span></label>
                                                        <div class="email-repeater form-group">
                                                            <div data-repeater-list="repeater-group">
                                                                <div data-repeater-item class="row m-b-15">
                                                                    <div class="col-md-8">
                                                                        <input type="datetime-local" class="form-control" id="date-rv">
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <button data-repeater-delete="" class="btn btn-danger waves-effect waves-light" type="button"><i class="ti-close"></i></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <button type="button" data-repeater-create="" class="btn btn-info waves-effect waves-light">Ajoutez une autre date
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <small>Les champs suivis de <span class="text-danger">*</span> sont obligatoires.</small>
                                    </section>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>  
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer" style="background-color: #001933;">
                <div class="row">
                    <!-- column -->
                    <div class="col-lg-12 col-md-12">
                        <div class="row p-5" >
                            <div class="col-xl-4 col-md-4">
                                <div class="row">
                                    <div class="col-xl-4 col-md-4 col-sm-6">
                                        <img src="../../assets/images/favicon-gendarmerie.png" alt="" width="100%">
                                    </div>
                                    <div class="col-xl-8 col-md-8 col-sm-6">
                                        <h4>Haut Commandement de la Gendarmerie Nationale</h4>
                                        <h6>Centre de Recherches et des Opérations de la Gendarmerie Nationale - CROGEND</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xl-4 col-md-4 col-sm-4"></div> 
                                    <div class="col-xl-8 col-md-8 col-sm-8">
                                        <br>
                                        <hr>
                                        <h4 class="mb-3">Brigade Numérique</h4>
                                        <p><a href="error-400.html">A Propos</a></p>
                                        <p><a href="faq.html">FAQ</a></p>
                                        <p><a href="error-400.html">Termes & Conditions</a></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-md-4 ml-5 pl-5">
                                <h4>Liens Utiles</h4>
                                <p><a href="https://www.sec.gouv.sn/">Gouvernement Sénégal</a></p>
                                <p><a href="https://forcesarmees.sec.gouv.sn/">Ministère des Forces Armées</a></p>
                                <p><a href="https://interieur.sec.gouv.sn/">Ministère de l'intérieur</a></p>
                                <p><a href="https://forcesarmees.sec.gouv.sn/">Ministère de la justice</a></p>
                                <p><a href="https://forcesarmees.sec.gouv.sn/">Armée Sénégalaise</a></p>
                                <p><a href="https://www.gendarmerie.sn/">Gendarmerie Nationale</a></p>
                            </div>
                            <div class="col-xl-3 col-md-3">
                                <h4 class="mb-1">Numéro Vert Gendarmerie :</h4>
                                <h4 style="color: #6a7a8c;">800 00 20 20</h4>
                                <br>
                                <button class="btn waves-effect btn-circle waves-light" type="button"> <i class="fab fa-facebook"></i> </button>
                                <button class="btn waves-effect btn-circle waves-light" type="button"> <i class="fab fa-twitter"></i> </button>
                                <button class="btn waves-effect btn-circle waves-light" type="button"> <i class="fab fa-whatsapp"></i> </button>
                                
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer text-center">
                &copy;2021 Gendarmerie Nationale Sénégalaise. Tous droits réservés.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <script src="../../dist/js/app.min.js"></script>
    <script src="../../dist/js/app.init.horizontal.js"></script>
    <script src="../../dist/js/app-style-switcher.horizontal.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="../../dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../../dist/js/custom.js"></script>
    <script src="../../assets/libs/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="../../assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>
    <!--This page JavaScript -->
    <script src="../../assets/libs/jquery.repeater/jquery.repeater.min.js"></script>
    <script src="../../assets/extra-libs/jquery.repeater/repeater-init.js"></script>
    <script src="../../assets/extra-libs/jquery.repeater/dff.js"></script>
    <script>
    //Basic Example
    $("#example-basic").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        autoFocus: true
    });

    // Basic Example with form
    var form = $("#example-form");
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
            confirm: {
                equalTo: "#password"
            }
        }
    });
    form.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        onStepChanging: function(event, currentIndex, newIndex) {
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onFinishing: function(event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function(event, currentIndex) {
            alert("Submitted!");
        }
    });

    // Advance Example

    var form = $("#example-advanced-form").show();

    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "slideLeft",
        onStepChanging: function(event, currentIndex, newIndex) {
            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex) {
                return true;
            }
            // Forbid next action on "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age-2").val()) < 18) {
                return false;
            }
            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex) {
                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onStepChanged: function(event, currentIndex, priorIndex) {
            // Used to skip the "Warning" step if the user is old enough.
            if (currentIndex === 2 && Number($("#age-2").val()) >= 18) {
                form.steps("next");
            }
            // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 2 && priorIndex === 3) {
                form.steps("previous");
            }
        },
        onFinishing: function(event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function(event, currentIndex) {
            alert("Submitted!");
        }
    }).validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
            confirm: {
                equalTo: "#password-2"
            }
        }
    });

    // Dynamic Manipulation
    $("#example-manipulation").steps({
        headerTag: "h3",
        bodyTag: "section",
        enableAllSteps: true,
        enablePagination: false
    });

    //Vertical Steps

    $("#example-vertical").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        stepsOrientation: "vertical"
    });

    //Custom design form example
    $(".tab-wizard").steps({
        headerTag: "h6",
        bodyTag: "section",
        transitionEffect: "fade",
        titleTemplate: '<span class="step">#index#</span> #title#',
        labels: {
            finish: "Submit"
        },
        onFinished: function(event, currentIndex) {
            swal("Form Submitted!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat eleifend ex semper, lobortis purus sed.");

        }
    });


    var form = $(".validation-wizard").show();

    $(".validation-wizard").steps({
        headerTag: "h6",
        bodyTag: "section",
        transitionEffect: "fade",
        titleTemplate: '<span class="step">#index#</span> #title#',
        labels: {
            finish: "Submit"
        },
        onStepChanging: function(event, currentIndex, newIndex) {
            return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
        },
        onFinishing: function(event, currentIndex) {
            return form.validate().settings.ignore = ":disabled", form.valid()
        },
        onFinished: function(event, currentIndex) {
            swal("Form Submitted!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat eleifend ex semper, lobortis purus sed.");
        }
    }), $(".validation-wizard").validate({
        ignore: "input[type=hidden]",
        errorClass: "text-danger",
        successClass: "text-success",
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass)
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass)
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element)
        },
        rules: {
            email: {
                email: !0
            }
        }
    })
    </script>
    <!-- ChatBot -->
    <script src="//code.tidio.co/h0wcezraw5v1k3cjeyichxyv1yzlcfsg.js" async></script>

</body>

</html>