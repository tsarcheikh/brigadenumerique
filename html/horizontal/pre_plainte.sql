-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : jeu. 25 mars 2021 à 17:34
-- Version du serveur :  5.7.31
-- Version de PHP : 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `pre_plainte`
--

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

CREATE TABLE `personne` (
  `id_personne` int(11) NOT NULL,
  `prenom` varchar(200) DEFAULT NULL,
  `nom` varchar(200) DEFAULT NULL,
  `date_naissance` date DEFAULT NULL,
  `lieu_naissance` varchar(250) DEFAULT NULL,
  `adresse` varchar(250) DEFAULT NULL,
  `telephone` varchar(250) DEFAULT NULL,
  `mail` varchar(250) DEFAULT NULL,
  `situation_familliale` varchar(250) DEFAULT NULL,
  `nationalite` varchar(250) DEFAULT NULL,
  `profession` varchar(250) DEFAULT NULL,
  `nom_epoux` varchar(250) DEFAULT NULL,
  `region` varchar(250) DEFAULT NULL,
  `departement` varchar(250) DEFAULT NULL,
  `commune` varchar(250) DEFAULT NULL,
  `sexe` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `personne`
--

INSERT INTO `personne` (`id_personne`, `prenom`, `nom`, `date_naissance`, `lieu_naissance`, `adresse`, `telephone`, `mail`, `situation_familliale`, `nationalite`, `profession`, `nom_epoux`, `region`, `departement`, `commune`, `sexe`) VALUES
(12, 'moussa', 'ndiaye', '2018-05-13', 'bargny', 'bargny', '88521455', 'pindiaye93@gmail.com', 'MariÃ©(e)', 'Senegalaise', 'gendarme', 'Seck', '', 'rufisque', 'bargny', 'Masculin');

-- --------------------------------------------------------

--
-- Structure de la table `plainte`
--

CREATE TABLE `plainte` (
  `id_plainte` int(11) NOT NULL,
  `id_personne` int(11) NOT NULL,
  `nature_plainte` varchar(250) NOT NULL,
  `qualite_plainte` varchar(250) NOT NULL,
  `faits` text NOT NULL,
  `date_faits` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `plainte`
--

INSERT INTO `plainte` (`id_plainte`, `id_personne`, `nature_plainte`, `qualite_plainte`, `faits`, `date_faits`) VALUES
(2, 12, '', 'Victime', '', '2008-05-13');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `personne`
--
ALTER TABLE `personne`
  ADD PRIMARY KEY (`id_personne`);

--
-- Index pour la table `plainte`
--
ALTER TABLE `plainte`
  ADD PRIMARY KEY (`id_plainte`),
  ADD KEY `id_personne` (`id_personne`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `personne`
--
ALTER TABLE `personne`
  MODIFY `id_personne` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `plainte`
--
ALTER TABLE `plainte`
  MODIFY `id_plainte` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
